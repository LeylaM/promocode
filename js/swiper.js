const swiper = new Swiper('.swiper-card', {
    // Optional parameters
    loop: true,
    spaceBetween:16,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true,
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

});

swiper.on('click', function () {
    swiper.slideNext();
});


const swiper1 = new Swiper('.swiper2', {
    loop: false,
    spaceBetween: 20,
    slidesPerView: 'auto',
});


const swiper2 = new Swiper('.swiper3', {
    loop: false,
    spaceBetween: 20,
    slidesPerView: 'auto',
});


/*Background gradient*/
let bodyGradient = document.getElementById('body-gradient');

//v1.0
// function changeBgGradientOnSlideChange(swiperImages) {
//     swiperImages.forEach(swiperImg => {
//         if (swiperImg.classList.contains('swiper-slide-active')) {
//             let bgGradient = swiperImg.dataset.gradientClass;
//
//             ['bg-gradient-classic', 'bg-gradient-gold', 'bg-gradient-platinum', 'bg-gradient-ring'].forEach(c => {
//                 bodyGradient.classList.remove(c);
//             })
//
//             bodyGradient.classList.add(bgGradient);
//         }
//     })
// }

// v1.1
// function changeBgGradientOnSlideChange(swiperImages) {
//     swiperImages.forEach(swiperImg => {
//         if (swiperImg.classList.contains('swiper-slide-active')) {
//             let bgGradient = swiperImg.dataset.gradientClass;
//
//             ['bg-gradient-classic', 'bg-gradient-gold', 'bg-gradient-platinum', 'bg-gradient-ring'].forEach(c => bodyGradient.classList.toggle(c, c === bgGradient))
//
//         }
//     })
// }

// v1.2
// function changeBgGradientOnSlideChange(swiperImages) {
//     const bgGradient = swiperImages.find( swiperImg => swiperImg.classList.contains('swiper-slide-active')).dataset.gradientClass;
//
//     ['bg-gradient-classic', 'bg-gradient-gold', 'bg-gradient-platinum', 'bg-gradient-ring'].forEach(c => bodyGradient.classList.toggle(c, c === bgGradient))
// }

// v1.Emin's favorite
function changeBgGradientOnSlideChange(swiperImages) {

    const activeSlideBgClass = swiperImages.find(swiperImg => swiperImg.classList.contains('swiper-slide-active')).dataset.gradientClass;

    const bgGradientClasses = new Set(swiperImages.map(swiperImg => swiperImg.dataset.gradientClass));

    bgGradientClasses.forEach(c => bodyGradient.classList.toggle(c, c === activeSlideBgClass));

}


/* Changing title on slide change */
let mainHeaders= document.querySelectorAll('.main-header');

function changeTitleOnSlideChange(swiperImages) {
    swiperImages.forEach(swiperImg => {
        if (swiperImg.classList.contains('swiper-slide-active')) {
            let activeSwiperTitle = document.querySelector(swiperImg.dataset.swiperImage);

            mainHeaders.forEach(mainHeader => {
                mainHeader.classList.add('hidden');
            })
            activeSwiperTitle.classList.remove('hidden');
        }
    })
}


swiper.on('slideChangeTransitionEnd', function () {
    let swiperImages = Array.from(document.querySelectorAll('div[data-swiper-image]'));
    changeTitleOnSlideChange(swiperImages);
    changeBgGradientOnSlideChange(swiperImages);
});

