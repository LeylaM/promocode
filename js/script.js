let burgerMenu = document.getElementById('header-mobile-nav');
let menu = document.getElementById('menu');
let menuBg = document.getElementById('menu-bg');

burgerMenu.addEventListener('click',() => {
    menu.classList.add('menu-visible');
})

let closeBtn = document.getElementById('mobile-nav-close-btn');

closeBtn.addEventListener('click', () => {
    menu.classList.remove('menu-visible');
    menuBg.classList.add('hidden');
})




let navItems= document.querySelectorAll("#header-nav .header-nav-item");

navItems.forEach( navItem => {
    navItem.addEventListener('click', (event) => {
        menu.classList.add('menu-visible');
        menuBg.classList.remove('hidden');
        let clickedTabContent = event.target.dataset.tabContent;
        menu.querySelector(`.header-mobile-nav-item[data-tab-content="${clickedTabContent}"]`).click();
    })
})


/*Tabs*/

function changeTabs(tabContainerId, contentContainerId){

    let tabs = [...document.getElementById(tabContainerId).children];
    let content = [...document.getElementById(contentContainerId).children];



    tabs.forEach( tab => {
        tab.addEventListener('click', (event) => {


            let activeTabContent = document.querySelector(tab.dataset.tabContent);


            tabs.forEach( tab => {
                tab.classList.remove('header-mobile-nav-item-active');
            });

            content.forEach( item => {
                item.classList.add('hidden');
            });

            event.target.classList.add('header-mobile-nav-item-active');
            activeTabContent.classList.remove('hidden');
        })
    })
}

changeTabs('header-open-nav', 'tab-content-container');









